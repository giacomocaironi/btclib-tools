---
template: "tool.html"
title: Bip32
---

### Bip 32 derivation 

<p>
<label>Seed</label>
<textarea id="seed" rows="1"></textarea>
</p>

<p>
<label>Extended master private key</label>
<textarea id="master_xprv" rows="1"></textarea>
</p>

<p>
<label>Extended master public key</label>
<textarea id="master_xpub" rows="1"></textarea>
</p>

<p>
<label>Derivation path</label>
<textarea id="path" rows="1"></textarea>
</p>

<p>
<label>Extended private key</label>
<textarea id="xprv" rows="1" readonly></textarea>
</p>

<p>
<label>Extended public key</label>
<textarea id="xpub" rows="1" readonly></textarea>
</p>

<button id="derive" disabled>Derive</button>
<button id="clear" disabled>Clear</button>
<button id="random" disabled>Random Seed</button>