---
template: "tool.html"
title: Address
---

### Random address

<p>
<label>P2PKH</label>
<textarea id="p2pkh" rows="1" readonly></textarea>
</p>

<p>
<label>P2SH</label>
<textarea id="p2sh" rows="1" readonly></textarea>
</p>

<p>
<label>P2WPKH</label>
<textarea id="p2wpkh" rows="1" readonly></textarea>
</p>

<p>
<label>P2WSH</label>
<textarea id="p2wsh" rows="1" readonly></textarea>
</p>

<p>
<label>P2TR</label>
<textarea id="p2tr" rows="1" readonly></textarea>
</p>

<button id="regenerate" disabled>Regenerate</button>