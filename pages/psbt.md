---
template: "tool.html"
title: Psbt
---

### Psbt parser

<textarea id="raw_psbt" rows="5"></textarea>

<button id="convert" disabled>Convert</button>

<div>
     <pre id="parsed_psbt">
     </pre>
</div>