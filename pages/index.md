---
template: "index.html"
title: Home
---

### Btclib Tools

This is a suite of online tools built on top of the [Btclib](https://github.com/btclib-org/btclib) library.

It was built by [Giacomo Caironi](https://www.giacomocaironi.dev)

This is the list of available tools:

* [Address]({{url("address")}}): Random bitcoin addresses
* [Psbt]({{url("psbt")}}): Psbt decoder
* [Bip32]({{url("bip32")}}): Derive bip32 keys from seed, master private key or master public key