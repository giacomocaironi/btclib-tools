from secrets import token_hex
from random import randint

from btclib.bip32.bip32 import rootxprv_from_seed, xpub_from_xprv, derive as derive_key
from js import document
from pyodide.ffi import create_proxy


def derive(*args):

    seed = document.querySelector("#seed").value
    master_xprv = document.querySelector("#master_xprv").value
    master_xpub = document.querySelector("#master_xpub").value
    if seed:
        master_xprv = rootxprv_from_seed(seed)
    if master_xprv:
        master_xpub = xpub_from_xprv(master_xprv)
    if not master_xpub:
        return

    path = document.querySelector("#path").value
    if master_xprv:
        xprv = derive_key(master_xprv, path)
        xpub = xpub_from_xprv(xprv)
    else:
        xpub = derive_key(master_xpub, path)
        xprv = ""

    document.querySelector("#seed").value = seed
    document.querySelector("#master_xprv").value = master_xprv
    document.querySelector("#master_xpub").value = master_xpub
    document.querySelector("#xprv").value = xprv
    document.querySelector("#xpub").value = xpub


def clear(*args):
    document.querySelector("#seed").value = ""
    document.querySelector("#master_xprv").value = ""
    document.querySelector("#master_xpub").value = ""
    # document.querySelector("#path").value = ""
    document.querySelector("#xprv").value = ""
    document.querySelector("#xpub").value = ""


def random(*args):
    clear()
    document.querySelector("#seed").value = token_hex(32)
    document.querySelector("#path").value = f"m/0h/0h/1h/{randint(0, 100)}"
    derive()


random()
proxy_derive = create_proxy(derive)
document.querySelector("#derive").addEventListener("click", proxy_derive)
document.querySelector("#derive").disabled = False
proxy_random = create_proxy(random)
document.querySelector("#random").addEventListener("click", proxy_random)
document.querySelector("#random").disabled = False
proxy_clear = create_proxy(clear)
document.querySelector("#clear").addEventListener("click", proxy_clear)
document.querySelector("#clear").disabled = False
