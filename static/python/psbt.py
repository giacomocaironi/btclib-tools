import textwrap
from pprint import PrettyPrinter

from btclib.psbt.psbt import Psbt
from js import document
from pyodide.ffi import create_proxy


class PsbtPrettyPrinter(PrettyPrinter):
    def _format(self, obj, *args, **kwargs):
        if isinstance(obj, bytes):
            obj = obj.hex()
        if isinstance(obj, dict):
            old_obj = obj
            obj = {}
            for key, value in old_obj.items():
                if isinstance(key, bytes):
                    key = key.hex()
                obj[key] = value
        if isinstance(obj, str) and len(obj) > 100:
            obj = "\n".join(textwrap.wrap(obj, 100))
        return super()._format(obj, *args, **kwargs)


def parse_psbt(*args):
    try:
        psbt_str = document.querySelector("#raw_psbt").value.replace("\n", "")
        content = PsbtPrettyPrinter(width=80, sort_dicts=False).pformat(
            Psbt.b64decode(psbt_str).to_dict()
        )
    except Exception as e:
        document.querySelector("#parsed_psbt").innerHTML = str(e)
        return
    document.querySelector("#parsed_psbt").innerHTML = content


proxy_parse_psbt = create_proxy(parse_psbt)
document.querySelector("#convert").addEventListener("click", proxy_parse_psbt)
document.querySelector("#convert").disabled = False
