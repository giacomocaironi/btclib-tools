from secrets import token_hex

from btclib.script.script_pub_key import ScriptPubKey
from js import document
from pyodide.ffi import create_proxy


def random_address(*args):
    document.querySelector("#p2pkh").value = ScriptPubKey.p2pkh(token_hex(32)).address
    document.querySelector("#p2sh").value = ScriptPubKey.p2sh(token_hex(32)).address
    document.querySelector("#p2wpkh").value = ScriptPubKey.p2wpkh(token_hex(32)).address
    document.querySelector("#p2wsh").value = ScriptPubKey.p2wsh(token_hex(32)).address
    document.querySelector("#p2tr").value = ScriptPubKey.p2tr(token_hex(32)).address


random_address()
proxy_random_address = create_proxy(random_address)
document.querySelector("#regenerate").addEventListener("click", proxy_random_address)
document.querySelector("#regenerate").disabled = False
