from kart import Kart, mappers, miners, modifiers, renderers
from kart.ext.markdown import markdown_to_html, markdown_to_toc
from kart.utils import date_to_string

kart = Kart(build_location="public")


kart.miners = [
    miners.DefaultDataMiner(),
    miners.DefaultPageMiner(),
]

kart.content_modifiers = []

kart.mappers = [
    mappers.DefaultPageMapper(),
    mappers.DefaultFeedMapper(collections=[]),
    mappers.DefaultSitemapMapper(),
    mappers.DefaultStaticFilesMapper(),
    mappers.DefaultRootDirMapper(),
]

kart.map_modifiers = []

kart.renderers = [
    renderers.DefaultSiteRenderer(
        filters={
            "html": markdown_to_html,
            # "toc": markdown_to_toc,
            "date_to_string": date_to_string,
        },
    ),
    renderers.DefaultFeedRenderer(),
    renderers.DefaultSitemapRenderer(),
    renderers.DefaultStaticFilesRenderer(),
    renderers.DefaultRootDirRenderer(),
]

kart.config["name"] = "Btclib tools"
kart.config["site_url"] = "https://btclib-tools.giacomocaironi.dev"
kart.config["pagination"] = {"per_page": 5, "skip": 1}
kart.config["timezone"] = "Europe/Rome"
kart.config["code_highlighting"] = {"style": "material"}

kart.run()
